FROM 95.217.213.216:1234/hardml_project_gateway

RUN pip install faiss-cpu==1.7.2 minio==7.1.12
COPY ./update_index.py ./update_index.py
COPY ./check_data.py ./check_data.py

CMD ["python", "update_index.py"]
