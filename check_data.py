import json
import os
import pickle

import faiss
import numpy as np
from sentence_transformers import SentenceTransformer


def check_data():
    """
    Проверка того, что файлы не битые и модель рабочая, чтобы обезопасить сервис от некорректной работы
    :return: None
    """
    try:
        # читаем файл с кластерами в память
        with open(os.environ["ROOT_FOLDER"] + "/" + os.environ["DOCUMENTS_FILE_PATH"], "r") as file:
            clusters = json.load(file)

        # загружаем модель
        model = SentenceTransformer(os.environ["MODEL_NAME"])

        # для проверки работы эмбеддера берём первый пример из первого кластера
        example = clusters[list(clusters.keys())[0]][0]
        embedding = model.encode(example)

        # загружаем центры кластеров
        with open(os.environ["ROOT_FOLDER"] + "/" + os.environ["CLUSTER_CENTRE_PATH"], "rb") as _file:
            res = pickle.load(_file)

        centres = np.array([val for _, val in res.items()])

        assert len(centres) == len(clusters), "Number of clusters and centres of clusters are different"
        assert len(centres[0]) == len(embedding), "Length of centre's embedding differs from length of model embedding"

        for i in clusters.keys():
            index = faiss.read_index(os.environ["ROOT_FOLDER"] + "/" + os.environ["INDEX_PATH"].format(i=i))

            example = clusters[i][0]
            embedding = model.encode(example).astype("float32")

            _, result = index.search(embedding.reshape(1, -1), 10)
            result = result[0]

            assert (len(result) > 0) and (result[0] != -1), "No similar objects are found, low quality index"

        print("ok")
    except Exception as e:
        print(str(e) if str(e) != "ok" else "Error")


if __name__ == "__main__":
    check_data()
