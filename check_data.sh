export PATH=$PATH:/root/minio-binaries/
. export_vars.sh

mc cp --recursive myminio/$S3_BUCKET/ $HOST_TEST_ROOT

docker run --rm -v $HOST_TEST_ROOT:$ROOT_FOLDER -e ROOT_FOLDER=$ROOT_FOLDER -e MODEL_NAME=$MODEL_NAME -e NLIST=$NLIST -e DOCUMENTS_FILE_PATH=$DOCUMENTS_FILE_PATH -e INDEX_PATH=$INDEX_PATH -e CLUSTER_CENTRE_PATH=$CLUSTER_CENTRE_PATH --entrypoint=python 95.217.213.216:1234/hardml_project_update_index check_data.py  > result.txt

RES=$(cat result.txt)
echo $RES

if [ "$RES" = "ok" ]
then
  rm -rf $HOST_ROOT
  mv $HOST_TEST_ROOT $HOST_ROOT
  rm -rf $HOST_TEST_ROOT
  rm result.txt
else
  mc cp $HOST_ROOT/* myminio/$S3_BUCKET/
  rm -rf $HOST_TEST_ROOT
  rm result.txt
  exit 1
fi
