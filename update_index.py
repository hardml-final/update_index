import gc
import json
import os
import pickle
import shutil

import faiss
from minio import Minio
from sentence_transformers import SentenceTransformer


def load_file_from_s3(s3_bucket, path_to_save):
    """
    Загрузить файл из s3
    :param s3_bucket: str, название бакета в minio
    :param path_to_save: str, путь, по которому нужно сохранить файл
    :return: None
    """
    response = client.get_object(s3_bucket, path_to_save)

    with open(path_to_save, "wb") as _file:
        shutil.copyfileobj(response, _file)

    response.close()
    response.release_conn()


# соединение
client = Minio(os.environ["S3_ENDPOINT"],
               access_key=os.environ["IAM_ACCESS_KEY"],
               secret_key=os.environ["IAM_SECRET_KEY"],
               secure=False)

# загружаем файл с кластерами
load_file_from_s3(os.environ["S3_BUCKET"], os.environ["DOCUMENTS_FILE_PATH"])

# читаем файл с кластерами в память
with open(os.environ["DOCUMENTS_FILE_PATH"], "r") as file:
    clusters = json.load(file)

# загружаем модель
model = SentenceTransformer(os.environ["MODEL_NAME"])

# параметр для кластера
nlist = int(os.environ["NLIST"])

# для каждого кластера делаем предсказания
# и бахаем индекс
# и ищем центр
centres = {}
for key, values in clusters.items():
    # получаем эмбеддинги
    embs = model.encode(values)
    embedding_size = embs.shape[1]

    # инициализируем индекс
    quantizer = faiss.IndexFlatL2(embedding_size)
    index = faiss.IndexIVFFlat(quantizer, embedding_size, nlist)

    # строим индекс
    index.train(embs)
    index.add(embs)

    # сохраняем индекс в файл
    index_path = os.environ["INDEX_PATH"].format(i=key)
    faiss.write_index(index, index_path)
    # и в облако
    client.fput_object(os.environ["S3_BUCKET"], index_path, index_path)

    centres[key] = embs.mean(axis=0)
    del embs
    gc.collect()

# записываем центры в файлы
with open(os.environ["CLUSTER_CENTRE_PATH"], "wb") as file:
    pickle.dump(centres, file)
# и в облако
client.fput_object(os.environ["S3_BUCKET"],
                   os.environ["CLUSTER_CENTRE_PATH"],
                   os.environ["CLUSTER_CENTRE_PATH"])

# выводим количество кластеров, чтобы записать его в файл-артефакт
print(str(len(clusters)))
